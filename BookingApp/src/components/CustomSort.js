import {StyleSheet, Text, View, Image, TouchableOpacity} from 'react-native';
import React, {useState} from 'react';
import {TextInput} from 'react-native-gesture-handler';

const CustomSort = ({placeholder, style}) => {
  const [text, setText] = useState('');
  const handleTextChange = newText => {
    setText(newText);
  };
  return (
    <View style={{backgroundColor: 'white', padding: 10, borderRadius: 15}}>
      <View style={styles.placeView}>
        <View>
          <View style={styles.container}>
            <Image
              source={require('../assets/icons/icon_location.png')}
              style={styles.iconLocation}
            />
            <Text style={styles.text}>Điểm đến, Khách sạn</Text>
          </View>
          <View style={styles.container}>
            <TextInput
              placeholder={placeholder}
              placeholderTextColor="#9F9F9F"
              value={text}
              style={styles.inputText}
              onChangeText={handleTextChange}
            />
          </View>
        </View>
        <Image
          style={styles.iconLocation}
          source={require('../assets/icons/icon_gps.png')}
        />
      </View>
      <View style={styles.dayView}>
        <View style={{flexDirection: 'row', alignItems: 'center'}}>
          <View>
            <View style={styles.container}>
              <TouchableOpacity>
                <Image
                  source={require('../assets/icons/icon_calendar.png')}
                  style={styles.iconLocation}
                />
              </TouchableOpacity>
              <Text style={styles.text}>Ngày Nhận Phòng</Text>
            </View>
            <View style={styles.container}>
              <Text style={styles.dayText}>aa</Text>
            </View>
          </View>
          <View>
            <View style={styles.container2}>
              <Text style={styles.text}>Số đêm nghỉ</Text>
            </View>
            <View style={styles.container}>
              <Text style={styles.dayText}>aa</Text>
            </View>
          </View>
        </View>
        <View style={{flexDirection: 'row', alignItems: 'center'}}>
          <Text>Ngay tra phong: </Text>
          <Text>aaa</Text>
        </View>
      </View>
    </View>
  );
};

export default CustomSort;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  container2: {
    flexDirection: 'row',
    alignItems: 'center',
    marginLeft: 10,
  },
  iconLocation: {
    height: 30,
    width: 30,
    tintColor: 'grey',
  },
  text: {
    fontSize: 17,
  },
  inputText: {
    flex: 1,
    height: 40,
    paddingHorizontal: 10,
    borderColor: 'grey',
    borderWidth: 1,
  },
  placeView: {
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomWidth: 1,
    borderColor: 'grey',
    height: 90,
    margin: 5,
  },
  dayView: {
    borderBottomWidth: 1,
    borderColor: 'grey',
    height: 75,
    margin: 5,
  },
  dayText: {
    fontWeight: 'bold',
    fontSize: 16,
  },
});
